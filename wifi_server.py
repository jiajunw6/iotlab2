import socket
from time import sleep
import picar_4wd as fc
import sys
import tty
import termios
import asyncio
from picamera import PiCamera
from io import BytesIO
import base64

def process(data,client,camera):
   print(data)
   
   if data==b'up':
      fc.forward(10)
      client.sendall(data)
   elif data==b'down':
      fc.backward(10)
      client.sendall(data)
   elif data==b'left':
      fc.turn_left(10)
      client.sendall(data)
   elif data==b'right':
      fc.turn_right(10)
      client.sendall(data)
   elif data==b'stop':
      fc.stop()
      client.sendall(data)
   elif data==b'status':
      distance=int(fc.get_distance_at(0))
      if(distance<0):
        distance='more than 2m'
      elif(distance<10):
        distance='less than 10cm'
        fc.backward(10)
        sleep(0.1)
        fc.stop()
      else:
        distance=str(distance)+' cm'
      status=distance
      client.sendall(bytes(status,'UTF-8'))
   elif data==b'photo':
      mystream=BytesIO()
      print('taking photo')
      camera.capture(mystream,'png')
      mystream.seek(0)
      photo=mystream.getvalue()
      if(photo):
        photo=bytes(str(base64.b64encode(photo),'UTF-8')+'*','UTF-8')
        print('sent'+str(len(photo)))
        client.sendall(photo)
      else :
        client.sendall('fail')
   else:
      client.sendall(data) # Echo back to client   
   return

if __name__ == '__main__':
    HOST = "192.168.8.88" # IP address of your Raspberry PI
    PORT = 65432          # Port to listen on (non-privileged ports are > 1023)
    camera=PiCamera()
    camera.resolution=(200,150)
    camera.start_preview()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        s.bind((HOST, PORT))
        s.listen()

        while 1:
            client, clientInfo = s.accept()
            while 1:
                try:
                    data = client.recv(1024)      # receive 1024 Bytes of message in binary format
                    if data != b"":
                        print("server recv from: ", clientInfo)
                        process(data,client,camera)                
                    else:                
                        client.close()
                        break
                except:
                    break
        print("Closing socket")
        s.close()

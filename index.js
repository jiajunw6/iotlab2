document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 65432;
var server_addr = "192.168.8.88";   // the IP address of your Raspberry PI

function send_command(input) {    
    const net = require('net');

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${input}`);
    });
    
    // get the data from the server
    if(input=='status') {
        client.on('data', (output) => {
            arr=output.toString().split(',');
            document.getElementById("distance").innerHTML=arr[0];
            client.end();
            client.destroy();
        });
    } else if(input=='photo') {
        window.photodata='';
        client.on('data', (output) => {
            console.log(output.toString());
            console.log(output.toString().length);
            photodata=photodata+output.toString().replace(/[\n\r\s\*]/g,'');
            if(output.toString().indexOf('*')!=-1) {
            	document.getElementById("photo").src='data:image/png;base64,'+window.photodata;
		    client.end();
		    client.destroy();
	     }
        });
    } else {
    	client.on('data', (output) => {
	    document.getElementById("msg").innerHTML = output.toString();
	    console.log(output.toString());
            client.end();
            client.destroy();
        });
    }
    

    client.on('end', () => {
        console.log('disconnected from server');
    });

}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;
    if (e.repeat) return
    
    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        send_command("up");
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        send_command("down");
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        send_command("left");
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        send_command("right");
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;
    send_command("stop");
    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}



setInterval(function(){
        send_command('status');
}, 500);

setInterval(function(){
        send_command('photo');
}, 800);
